package br.com.master.api.ponto.models;

import br.com.master.api.ponto.enums.TipoBatidaPontoEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
public class Ponto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Double totalDeHoras;

    @ManyToOne
    @JsonIgnore
    private Usuario usuario;

    private LocalDateTime dataHora;

    @NotNull
    private TipoBatidaPontoEnum tipoBatida;


    public Ponto() {
    }

    public Double getTotalDeHoras() {
        return totalDeHoras;
    }

    public void setTotalDeHoras(Double totalDeHoras) {
        this.totalDeHoras = totalDeHoras;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public LocalDateTime getDataHora() {
        return dataHora;
    }

    public void setDataHora(LocalDateTime dataHora) {
        this.dataHora = dataHora;
    }

    public TipoBatidaPontoEnum getTipoBatida() {
        return tipoBatida;
    }

    public void setTipoBatida(TipoBatidaPontoEnum tipoBatida) {
        this.tipoBatida = tipoBatida;
    }
}
