package br.com.master.api.ponto.controllers;

import br.com.master.api.ponto.models.Ponto;
import br.com.master.api.ponto.models.Usuario;
import br.com.master.api.ponto.models.dtos.PontoDTO;
import br.com.master.api.ponto.services.PontoService;
import br.com.master.api.ponto.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/usuarios")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private PontoService pontoService;

    @PostMapping
    public ResponseEntity<Usuario> registrar(@RequestBody @Valid Usuario usuario){
        try{
            Usuario usuarioObjeto = usuarioService.registrar(usuario);
            return ResponseEntity.status(201).body(usuarioObjeto);
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @PostMapping("/{id}/ponto")
    public ResponseEntity<Ponto> registrar(@PathVariable(name="id") Long id,
                                             @RequestBody @Valid Ponto ponto){
        try{
            Ponto pontoObjeto = pontoService.registrar(id, ponto);
            return ResponseEntity.status(201).body(pontoObjeto);
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @GetMapping
    public Iterable<Usuario> buscarTodos(){
        Iterable<Usuario> usuarios = usuarioService.buscarTodos();
        return usuarios;
    }

    @GetMapping("/{id}")
    public Usuario buscarPorId(@PathVariable(name = "id") Long id){
        try {
            Usuario usuario = usuarioService.buscarPorId(id);
            return usuario;
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @GetMapping("/{id}/ponto")
    public PontoDTO buscarPontoPorUsuario(@PathVariable(name = "id") Long id){
        try {
            Usuario usuario = usuarioService.buscarPorId(id);
            PontoDTO pontos = pontoService.buscarPontoPorUsuario(id);
            return pontos;
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Usuario atualizar(@PathVariable(name = "id") Long id, @RequestBody @Valid Usuario usuario){
        try {
            Usuario usuarioObjeto = usuarioService.atualizar(id, usuario);
            return usuarioObjeto;
        }catch (RuntimeException exeception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exeception.getMessage());
        }
    }
}
