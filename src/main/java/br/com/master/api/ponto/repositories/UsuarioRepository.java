package br.com.master.api.ponto.repositories;

import br.com.master.api.ponto.models.Usuario;
import org.springframework.data.repository.CrudRepository;

public interface UsuarioRepository extends CrudRepository<Usuario, Long> {
    Iterable<Usuario> findAllByNomeCompletoContaining(String nome);
    Iterable<Usuario> findByEmailContaining(String email);
    Usuario findByEmail(String email);
    Usuario findByCpf(String cpf);
}
