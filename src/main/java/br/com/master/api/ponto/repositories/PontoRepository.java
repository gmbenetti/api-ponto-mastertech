package br.com.master.api.ponto.repositories;

import br.com.master.api.ponto.models.Ponto;
import org.springframework.data.repository.CrudRepository;

public interface PontoRepository extends CrudRepository<Ponto, Long> {
    Iterable<Ponto> findAllByUsuarioId(Long idUsuario);
}
