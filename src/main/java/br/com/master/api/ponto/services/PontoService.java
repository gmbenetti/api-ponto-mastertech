package br.com.master.api.ponto.services;

import br.com.master.api.ponto.enums.TipoBatidaPontoEnum;
import br.com.master.api.ponto.models.Ponto;
import br.com.master.api.ponto.models.Usuario;
import br.com.master.api.ponto.models.dtos.PontoDTO;
import br.com.master.api.ponto.repositories.PontoRepository;
import br.com.master.api.ponto.repositories.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

@Service
public class PontoService {

    @Autowired
    private PontoRepository pontoRepository;

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private UsuarioService usuarioService;

    public Ponto registrar(Long idUsuario, Ponto ponto){
        Usuario usuario = usuarioService.buscarPorId(idUsuario);
        ponto.setUsuario(usuario);
        ponto.setDataHora(LocalDateTime.now());
        Ponto pontoObjeto = pontoRepository.save(ponto);
        return pontoObjeto;
    }

    public PontoDTO buscarPontoPorUsuario(Long idUsuario){
        Usuario usuario = usuarioService.buscarPorId(idUsuario);
        List<Ponto> pontos = (List<Ponto>) pontoRepository.findAllByUsuarioId(idUsuario);

        Long segundosTrabalhados =  calculaSegundosTrabalhadas(pontos);
        Double horasTrabalhadas = 0.0;

        if(segundosTrabalhados != -1) {
            horasTrabalhadas = segundosTrabalhados.doubleValue() / 3600;
        }else{
            horasTrabalhadas = -1.0;
        }
        System.out.println("total de horas trabalhadas: " + horasTrabalhadas);

        PontoDTO pontoDTO = new PontoDTO(pontos, horasTrabalhadas);

        return pontoDTO;
    }

    public Long calculaSegundosTrabalhadas(List<Ponto> pontos){
        Integer totalDePontos = pontos.size();
        Long tempoTotal = 0L;

        if((totalDePontos % 2) == 0){
            for(int i = totalDePontos-1; i > 0; i--){
                if(pontos.get(i).getTipoBatida().equals(TipoBatidaPontoEnum.SAIDA)){
                    Long tempo = 0L;
                    LocalDateTime entrada;
                    LocalDateTime saida;

                    saida = pontos.get(i).getDataHora();
                    entrada = pontos.get(i-1).getDataHora();

                    tempo = ChronoUnit.SECONDS.between(entrada, saida);
                    tempoTotal +=  tempo;
                }
            }
        }else{
            return -1L;
        }
        return tempoTotal;
    }
}
