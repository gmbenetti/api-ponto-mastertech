package br.com.master.api.ponto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiPontoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiPontoApplication.class, args);
	}

}
