package br.com.master.api.ponto.models.dtos;

import br.com.master.api.ponto.models.Ponto;

import java.util.List;

public class PontoDTO {


    private Double totalDeHoras;

    private List<Ponto> pontosDTO;

    public PontoDTO() {
    }

    public PontoDTO(List<Ponto> pontos, Double totalDeHoras) {
        this.setPontos(pontos);
        this.setTotalDeHoras(totalDeHoras);
    }

    public Double getTotalDeHoras() {
        return totalDeHoras;
    }

    public void setTotalDeHoras(Double totalDeHoras) {
        this.totalDeHoras = totalDeHoras;
    }

    public List<Ponto> getPontos() {
        return pontosDTO;
    }

    public void setPontos(List<Ponto> pontos) {
        this.pontosDTO = pontos;
    }

}
