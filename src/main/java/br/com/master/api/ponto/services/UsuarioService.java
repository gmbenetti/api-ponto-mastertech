package br.com.master.api.ponto.services;

import br.com.master.api.ponto.models.Usuario;
import br.com.master.api.ponto.repositories.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Optional;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    public Usuario registrar(Usuario usuario) {

        Usuario usuarioObjeto =  usuarioRepository.findByEmail(usuario.getEmail());
        if(usuarioObjeto == null){
            usuarioObjeto = usuarioRepository.findByCpf(usuario.getCpf());
            if(usuarioObjeto != null){
                throw new RuntimeException("CPF " + usuario.getCpf() + " já cadastrado");
            }
        }else{
            throw new RuntimeException("Email " + usuario.getEmail() +" já cadastrado");
        }

        LocalDate dataCadastro = LocalDate.now();
        usuario.setDataCadastro(dataCadastro);

        usuarioObjeto = usuarioRepository.save(usuario);
        return usuarioObjeto;
    }

    public Usuario atualizar(Long id, Usuario usuario) {
        Usuario usuarioObjeto = buscarPorId(id);

        Usuario usuarioOptionalCpf = usuarioRepository.findByCpf(usuario.getCpf());
        if (usuarioOptionalCpf != null && (id != usuarioObjeto.getId())) {
            throw new RuntimeException("O cpf informado já existe no cadastro e pertence à outro usuário.");
        }

        usuarioObjeto.setCpf(usuario.getCpf());
        usuarioObjeto.setEmail(usuario.getEmail());
        usuarioObjeto.setNomeCompleto(usuario.getNomeCompleto());

        return usuarioRepository.save(usuarioObjeto);
    }

    public Iterable<Usuario> buscarTodos() {
        Iterable<Usuario> usuarios = usuarioRepository.findAll();
        return usuarios;
    }

    public Usuario buscarPorId(Long id) {
        Optional<Usuario> usuarioOptional = usuarioRepository.findById(id);
        if (usuarioOptional.isPresent()) {
            Usuario usuario = usuarioOptional.get();
            return usuario;
        } else {
            throw new RuntimeException("O id de usuário " + id + " não está cadastrado.");
        }
    }

    public Usuario buscarPorEmail(String email){
        Usuario usuario = usuarioRepository.findByEmail(email);
        if(usuario != null){
            return usuario;
        }
        throw new RuntimeException("Email " + email + " não cadastrado.");
    }
}
