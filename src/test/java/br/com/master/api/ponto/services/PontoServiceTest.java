package br.com.master.api.ponto.services;

import br.com.master.api.ponto.enums.TipoBatidaPontoEnum;
import br.com.master.api.ponto.models.Ponto;
import br.com.master.api.ponto.models.Usuario;
import br.com.master.api.ponto.repositories.PontoRepository;
import br.com.master.api.ponto.repositories.UsuarioRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static br.com.master.api.ponto.enums.TipoBatidaPontoEnum.ENTRADA;

@SpringBootTest
public class PontoServiceTest {

    @MockBean
    private PontoRepository pontoRepository;

    @MockBean
    private UsuarioService usuarioService;

    @Autowired
    private PontoService pontoService;

    private Ponto pontoEntrada;
    private Ponto pontoSaida;
    private Usuario usuario;
    private List<Ponto> pontos;


    @BeforeEach
    public void setUp(){
        usuario = new Usuario();
        usuario.setId((long) 1);
        usuario.setCpf("79274560087");
        usuario.setNomeCompleto("Usuario Teste");
        usuario.setEmail("teste@teste.com");

        pontoEntrada = new Ponto();
        pontoEntrada.setId((long)1);
        pontoEntrada.setDataHora(LocalDateTime.now());
        pontoEntrada.setTipoBatida(ENTRADA);
        pontoEntrada.setUsuario(usuario);

        pontoSaida = new Ponto();
        pontoSaida.setId((long)2);
        pontoSaida.setDataHora(LocalDateTime.now().plus(8, ChronoUnit.HOURS));
        pontoSaida.setTipoBatida(TipoBatidaPontoEnum.SAIDA);
        pontoSaida.setUsuario(usuario);

    }

    @Test
    public void testarRegistrar(){
        Mockito.when(usuarioService.buscarPorId(Mockito.anyLong())).thenReturn(usuario);
        Mockito.when(pontoRepository.save(Mockito.any(Ponto.class))).thenReturn(pontoEntrada);

        Ponto pontoObjeto = pontoService.registrar((long)0,pontoEntrada);

        Assertions.assertEquals(ENTRADA, pontoObjeto.getTipoBatida());
    }
}
