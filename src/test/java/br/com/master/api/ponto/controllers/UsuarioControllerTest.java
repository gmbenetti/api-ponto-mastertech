package br.com.master.api.ponto.controllers;

import br.com.master.api.ponto.models.Ponto;
import br.com.master.api.ponto.models.Usuario;
import br.com.master.api.ponto.services.PontoService;
import br.com.master.api.ponto.services.UsuarioService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@WebMvcTest(UsuarioController.class)
public class UsuarioControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UsuarioService usuarioService;

    @MockBean
    private PontoService pontoService;

    private Usuario usuario;
    private List<Ponto> pontos;

    @BeforeEach
    public void setUp(){
        usuario = new Usuario();
        usuario.setId((long) 1);
        usuario.setCpf("79274560087");
        usuario.setNomeCompleto("Usuario Teste");
        usuario.setEmail("teste@teste.com");
    }


    @Test
    public void testarBuscarTodosSemParametroValido() throws Exception{
        Mockito.when(usuarioService.buscarTodos()).then(usuarios -> {
           List<Usuario> usuariosObjeto = new ArrayList<>();
           usuariosObjeto.add(usuario);
           return usuariosObjeto;
        });

        mockMvc.perform(MockMvcRequestBuilders.get("/usuarios")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is(200));

    }

    @Test
    public void testarBuscarPorIdValido() throws Exception{
        Mockito.when((usuarioService.buscarPorId(Mockito.anyLong()))).then(usuarios -> {
           usuario.setId((long)1);
           usuario.setDataCadastro(LocalDate.now());
           return  usuario;
        });

        mockMvc.perform(MockMvcRequestBuilders.get("/usuarios/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.dataCadastro", CoreMatchers.equalTo(LocalDate
                        .now().toString())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.nomeCompleto", CoreMatchers.any(String.class)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.email", CoreMatchers.any(String.class)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.cpf", CoreMatchers.any(String.class)));
    }

    @Test
    public void testarBucarPorIdInvalido() throws Exception {

        Mockito.when(usuarioService.buscarPorId(Mockito.anyLong())).thenThrow(RuntimeException.class);

        mockMvc.perform(MockMvcRequestBuilders.get("/usuarios/99")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testarAtualizarValido() throws Exception{
        Mockito.when(usuarioService.atualizar(Mockito.anyLong(),Mockito.any(Usuario.class))).then(usuarios -> {
           usuario.setId((long)1);;
           usuario.setDataCadastro(LocalDate.now());
           return usuario;
        });

        ObjectMapper mapper = new ObjectMapper();
        String jsonDeUsuario = mapper.writeValueAsString(usuario);

        mockMvc.perform(MockMvcRequestBuilders.put("/usuarios/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeUsuario))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.dataCadastro", CoreMatchers.equalTo(LocalDate
                        .now().toString())));
    }

    @Test
    public void testarRegistrarValido() throws Exception{
        Mockito.when(usuarioService.registrar(Mockito.any(Usuario.class))).then(usuarioObjeto -> {
            usuario.setId((long)1);
            usuario.setDataCadastro(LocalDate.now());
            return usuario;
        });

        ObjectMapper mapper = new ObjectMapper();
        String jsonDeUsuario = mapper.writeValueAsString(usuario);

        mockMvc.perform(MockMvcRequestBuilders.post("/usuarios")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeUsuario))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.dataCadastro", CoreMatchers.equalTo(LocalDate.now()
                        .toString())));
    }


}
