package br.com.master.api.ponto.services;


import br.com.master.api.ponto.models.Ponto;
import br.com.master.api.ponto.models.Usuario;
import br.com.master.api.ponto.repositories.UsuarioRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class UsuarioServiceTest {

    @MockBean
    private UsuarioRepository usuarioRepository;

    @Autowired
    private UsuarioService usuarioService;

    private Usuario usuario;
    private List<Ponto> pontos;

    @BeforeEach
    public void setUp(){
        usuario = new Usuario();
        usuario.setId((long) 1);
        usuario.setCpf("79274560087");
        usuario.setNomeCompleto("Usuario Teste");
        usuario.setEmail("teste@teste.com");
    }

    @Test
    public void testarRegistrar(){

        Usuario usuarioTeste = new Usuario();
        usuarioTeste.setEmail("teste2@email.com.br");
        usuarioTeste.setNomeCompleto("Teste Registro");
        usuarioTeste.setCpf("63630423051");

        Mockito.when(usuarioRepository.save(usuarioTeste)).thenReturn(usuarioTeste);

        Usuario usuarioObjeto = usuarioService.registrar(usuarioTeste);

        Assertions.assertEquals(LocalDate.now(), usuarioObjeto.getDataCadastro());
        Assertions.assertEquals("teste2@email.com.br", usuarioObjeto.getEmail());
        Assertions.assertEquals("63630423051", usuarioObjeto.getCpf());
        Assertions.assertEquals("Teste Registro", usuarioObjeto.getNomeCompleto());
    }

    @Test
    public void testarRegistrarEmailExistente(){
        Mockito.when(usuarioRepository.findByEmail("teste@teste.com")).thenReturn(usuario);

        Assertions.assertThrows(RuntimeException.class, () -> {
            Usuario usuarioTeste = new Usuario();
            usuarioTeste.setEmail("teste@teste.com");
            usuarioTeste.setNomeCompleto("Teste Registro");
            usuarioTeste.setCpf("63630423051");
            Usuario usuarioObjeto = usuarioService.registrar(usuarioTeste);
        });
    }

    @Test
    public void testarRegistrarCpfExistente(){
        Mockito.when(usuarioRepository.findByEmail("teste@teste.com")).thenReturn(null);
        Mockito.when(usuarioRepository.findByCpf("79274560087")).thenReturn(usuario);

        Assertions.assertThrows(RuntimeException.class, () -> {
            Usuario usuarioTeste = new Usuario();
            usuarioTeste.setEmail("teste99@teste.com");
            usuarioTeste.setNomeCompleto("Teste Registro");
            usuarioTeste.setCpf("79274560087");
            Usuario usuarioObjeto = usuarioService.registrar(usuarioTeste);
        });
    }

    @Test
    public void testarAtualizar(){
        Mockito.when(usuarioRepository.findById((long)1)).thenReturn(Optional.ofNullable(usuario));

        usuario.setEmail("teste2@email.com.br");
        usuario.setNomeCompleto("Teste Registro");
        usuario.setCpf("63630423051");

        Mockito.when(usuarioRepository.save(usuario)).thenReturn(usuario);

        Usuario usuarioObjeto = usuarioService.atualizar((long) 1,usuario);

        Assertions.assertEquals(usuario.getDataCadastro(), usuarioObjeto.getDataCadastro());
        Assertions.assertEquals("teste2@email.com.br", usuarioObjeto.getEmail());
        Assertions.assertEquals("63630423051", usuarioObjeto.getCpf());
        Assertions.assertEquals("Teste Registro", usuarioObjeto.getNomeCompleto());
    }

    @Test void testarBuscarTodos(){
        Iterable<Usuario> usuarios = Arrays.asList(usuario);
        Mockito.when(usuarioRepository.findAll()).thenReturn(usuarios);

        Iterable<Usuario> usuarioIterable = usuarioService.buscarTodos();

        Assertions.assertEquals(usuarios, usuarioIterable);
    }
}
